import React from "react";
import ReactDOM from "react-dom";

import "./index.scss";
import Layout from "home/Layout";
import Navbar from "home/Navbar";
import ContentDetail from "./ContentDetail"

const App = () => (
  <>
    <Navbar />
    <Layout>
      <ContentDetail />
    </Layout>
  </>
);
ReactDOM.render(<App />, document.getElementById("app"));
